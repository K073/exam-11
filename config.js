module.exports = {
  db : {
    url : 'mongodb://localhost:27017',
    name : 'store'
  },
  jwt: {
    lifetime: 6400000,
    secret: 'pizdecsecret'
  }
};