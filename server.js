const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const config = require('./config');

const app = express();

const port = process.env.PORT || 8000;

const users = require('./controllers/auth/Users');
const products = require('./controllers/store/products');

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', () => {
  console.log('Mongoose connected!');

  app.use('/user', users());
  app.use('/products', products());

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});
