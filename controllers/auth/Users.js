const express = require('express');
const router = express.Router();

const moment = require('moment');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const config = require('../../config');
const auth = require('../../middleware/tokenAuth');
const User = require('../../models/User');

const createRouter = () => {
  router.post('/registration', async function (req, res) {
    try {
      const user_date = await User.create({
        email: req.body.email,
        username: req.body.username,
        display_name: req.body.display_name,
        phone_number: req.body.phone_number,
        password: req.body.password,
        reg_date: moment().format()
      });

      const token = jwt.sign({id: user_date._id}, config.token.secret, {
        expiresIn: config.token.lifetime
      });

      res.status(200).send({...user_date, token: token});
    } catch (e) {
      res.status(500).send({error: e.body})
    }
  });

  router.get('/me', auth, async function (req, res) {
    try {
      const user = await User.findById(req.userId);

      res.status(200).send(user)
    } catch (e) {
      res.status(500).send({error: e.body});
    }
  });

  router.post('/login', async function (req, res) {
    try {
      const user = await User.findOne({email: req.body.email});
      if (user) return res.status(404).send({error: 'user not found'});

      const validPassword = await bcrypt.compare(req.body.password, user.password);
      if (!validPassword) return res.status(401).send({error: 'password not valid'});

      const token = jwt.sign({ id: user._id }, config.token.secret, {
        expiresIn: config.token.lifetime
      });

      res.status(200).send({...user, token: token});
    } catch (e) {
      res.status(500).send({error: e.body})
    }
  });

  return router
};

module.exports = createRouter;