const express = require('express');
const router = express.Router();

const auth = require('../../middleware/tokenAuth');
const User = require('../../models/User');
const Product = require('../../models/Product');

const CreateRouter = () => {
  router.post('/create', auth, async function (req, res) {
    try {
      const user = await User.findById(req.userId);

      const product = await Product.create({
        user: user,
        title: req.body.title,
        description: req.body.description,
        img: req.body.img,
        category: req.body.category
      });

      res.status(200).send(product)
    } catch (e) {
      res.status(400).send({error: e.body})
    }
  });

  router.get('/:id', async function (req, res) {
    try {
      const products = req.params.id
        ? await Product.findById(req.params.id)
        : await Product.find();

      res.status(200).send(products)
    } catch (e) {
      res.status(400).send({error: e.body})
    }
  });

  router.get('/remove/:id', auth, async function (req, res) {
    try {
      const product = Product.findOne({_id: req.params.id})
      if (product._id !== req.userId) return res.status(300).send({error: 'permission deny'})

      product.remove();
      res.status(200).send({status: true});
    } catch (e) {
      res.status(400).send({error: e.body});
    }
  });

  return router;
};

module.exports = CreateRouter;