const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 8;

const UserSchema = new mongoose.Schema({
  username: {
    type: String, required: true
  },
  email: {
    type: String, required: true, unique: true
  },
  password: {
    type: String, required: true
  },
  phone_number: {
    type: Number, required: true
  },
  display_name: {
    type: Number, required: true
  }
});

UserSchema.pre('save', async function(next) {
  const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
  this.password = await bcrypt.hash(this.password, salt);
  next();
});

UserSchema.set('toJSON', {
  transform: (doc, ret, options) => {
    delete ret.password;
    return ret;
  }
});

mongoose.model('User', UserSchema);

module.exports = mongoose.model('User');