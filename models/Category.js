const mongoose = require('mongoose');

const CategorySchema = new mongoose.Schema({
  name: {
    type: String, required: true, unique: true
  },
});

mongoose.model('Category', CategorySchema);

module.exports = mongoose.model('Category');