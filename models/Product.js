const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  title: {
    type: String, required: true, unique: true
  },
  description: {
    type: String, required: true
  },
  img_url: {
    type: String, required: true
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'category',
    required: true
  }
});

mongoose.model('Product', ProductSchema);

module.exports = mongoose.model('Product');